import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7/screens/home/body.dart';

// REFERENSI: https://www.youtube.com/watch?v=XBKzpTz65Io
class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppbar(),
        body: Body(),
    );
  }

  AppBar buildAppbar() {
    return AppBar(
       leading: Icon(Icons.arrow_back),
          title: Text(widget.title, style: TextStyle(color: Colors.yellow, fontWeight: FontWeight.bold)),
          centerTitle: true,
          actions: <Widget>[
            IconButton(onPressed: null,
                icon: new Icon(Icons.search, color: Colors.white)),
          ],
    );
  }


}


