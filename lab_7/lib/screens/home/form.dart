import 'package:flutter/material.dart';
import 'package:lab_7/constant.dart';
import 'package:lab_7/screens/home/home_page.dart';
// REFERENSI: https://docs.flutter.dev/cookbook/forms/validation
class FormWidget extends StatefulWidget {

  @override
  FormWidgetState createState() => new FormWidgetState();
  
}

class FormWidgetState extends State<FormWidget> {
final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambahkan Kamus Onat", style: TextStyle(color: Colors.yellow, fontWeight: FontWeight.bold)),
        centerTitle: true,
         
      ),
      body: buildForm(context),
      
    );
  }

  Widget buildForm(BuildContext context) {
    
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(
                  hintStyle: TextStyle(fontSize: 18.0, fontFamily: 'Lato'),
                  hintText: 'Foto Obat'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Masukkan link foto obat!';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(
                  hintStyle: TextStyle(fontSize: 18.0, fontFamily: 'Lato'),
                  hintText: 'Nama Obat'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Masukkan nama obat!';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(
                  hintStyle: TextStyle(fontSize: 18.0, fontFamily: 'Lato'),
                  hintText: 'Deskripsi Obat'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Masukkan deskripsi obat!';
                }
                return null;
              },
            ),
          
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: 
                    ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Kamus Obat berhasil ditambah!')),
                          );
                        }
                      },
                      child: const Text('Submit'),
                    ),
            ),
          ],
        ),
      ),

    );
  }  
}
