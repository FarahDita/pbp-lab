import 'package:flutter/material.dart';
import 'package:lab_7/constant.dart';
import 'package:lab_7/models/products.dart';
import 'package:lab_7/screens/details/body.dart';
// import 'package:lab_6/screens/details/components/body.dart';

class DetailPage extends StatelessWidget {
  final Medicine medicine;

  const DetailPage({required this.medicine});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // each product have a color
      backgroundColor: medicine.color,
      appBar: AppBar(
        backgroundColor: medicine.color,
        elevation: 0,
        // actions: <Widget>[
        //   IconButton(icon: new Icon(Icons.arrow_back),
        //   onPressed: (){} ,),
        // SizedBox(width: kDefaultPaddin,)
        // ],
        ),
      body: Body(medicine: medicine),
    );
  }
}

  