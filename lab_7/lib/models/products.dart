import 'package:flutter/material.dart';

class Medicine {
  final String image, title, description, text;
  final Color color;
  final int id;
  Medicine({
    required this.image,
    required this.title,
    required this.text,
    required this.description,
    required this.color,
    required this.id,
  });
}

List<Medicine> medicines = [
  Medicine(
    id: 1,
    title: "Paramex",
    text : "Obat Pereda Sakit Kepala",
    description: dummyText,
    image: "https://images.tokopedia.net/img/cache/700/product-1/2020/2/7/391219/391219_aaa80ff8-792f-4aa4-9c07-bc16f718207d_554_554.jpg",
    color: Color(0xFFE6B398)),

  Medicine(
    id: 2,
    title: "Sanmol",
    text : "Obat Penurun Demam",
    description: dummyText,
    image: "https://id-live-02.slatic.net/p/b471c419cedbe15c6f6e3d9fd030b9ed.jpg",
    color: Color(0xFFFB7883)),

  Medicine(
    id: 3,
    title: "Salonpas",
    text: "Koyo Pereda Nyeri",
    description: dummyText,
    image: "https://assets.klikindomaret.com/share/10034123_1.jpg",
    color: Color(0xFFAEAEAE)),

  Medicine(
    id: 4,
    title: "Balsem",
    text: "Balsem Pelemas Otot Tegang",
    description: dummyText,
    image: "https://id-live-02.slatic.net/p/4bf7b83022770f1d0a282ccb4c98a817.jpg",
    color: Color(0xFFE6B398)),
  Medicine(
    id: 5,
    title: "Paracetamol",
    text: "Obat Flue",
    description: dummyText,
    image: "https://images.k24klik.com/product/large/apotek_online_k24klik_20210624013902359225_paracetamol-triman.jpg",
    color: Color(0xFFFB7883)),
  Medicine(
    id: 6,
    title: "Formula 44",
    text: "Obat Batuk Berdahak",
    description: dummyText,
    image: "https://images.tokopedia.net/img/cache/700/product-1/2019/12/23/5273754/5273754_a5f1f75c-2b7c-42e6-9391-c9765f18b78d_384_384.jpg",
    color: Color(0xFFAEAEAE),
  ),
];

String dummyText =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempus magna a efficitur varius. Curabitur pretium augue hendrerit tortor congue imperdiet. Morbi volutpat est a vehicula vestibulum. Donec efficitur vestibulum lectus id malesuada. Nullam in ipsum ante. Vestibulum pharetra elit dolor, id facilisis urna ultrices vitae.";