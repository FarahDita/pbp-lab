from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponseRedirect
# melakukan convert object dengan serializer ke bentuk yang dapat ditransfer melalui internet
# menggunakan HTTP antara client dan server
from django.core import serializers

from lab_4.forms import NoteForm

# Create your views here.

# Menampilkan data dalam bentuk HTML
def index(request):
    notes = Note.objects.all().values()
    response = {"notes" : notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
    form = NoteForm(request.POST or None)
    ## mengecek terlebih dahulu apakah form yang dimasukkan user valid dan method yang diminta adalah POST
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4') ## Setelah submit halaman akan di redirect ke friend list

    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {"notes" : notes}
    return render(request, "lab4_note_list.html", response)


