from datetime import datetime
from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    ## menghubungkan logika kode yang mana fields dari models dapat dohubungkan ke widget pada forms
    class Meta:
        model = Note
        fields = '__all__'
    

    ## Membuat attribut untuk setiap field
    input_untuk = {
		'type' : 'text',
		'placeholder' : 'Untuk siapa'
        }
    
    input_oleh = {
		'type' : 'text',
		'placeholder' : 'Dari siapa'
        }
    
    input_judul = {
		'type' : 'text',
		'placeholder' : 'Judul pesan kamu'
        }
    input_pesan = {
		'type' : 'textarea',
		'placeholder' : 'Isi pesan kamu'
        }
    ## Membuat field dan widget dalam bentuk forms
    untuk = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_untuk))
    oleh = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_oleh))
    judul = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_judul))
    pesan = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_pesan))

