# Melakukan import dari views untuk dapat mengambil method yang bersesuaian
from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    # membuat route path sesuai dengan permintaan pada soal untuk menampilkan:
    # HTML dengan method index
    path('', index, name='index_lab4'),
    path('add-note/', add_note, name='add_note'),
    path('note-list/', note_list, name='note_list'),
]