import 'package:flutter/material.dart';
import 'package:lab_6/constant.dart';
import 'package:lab_6/models/products.dart';

class Body extends StatelessWidget{
  final Medicine medicine;

  const Body({
    required this.medicine,
  });

  @override
  Widget build(BuildContext context){
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
    
      child: Column(
        children: <Widget>[
          SizedBox(
            height: size.height,
            child: Stack(
              children: <Widget>[
              
                Padding(
                  padding: EdgeInsets.all(kDefaultPaddin),
                  child: Column(
                    children: <Widget>[
                      Text(medicine.title, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 36),),
                      Row(
                        children: <Widget>[
                          Text(medicine.text, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 18,)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          SizedBox(width: kDefaultPaddin,),
                          Expanded(
                            child: Image.network(medicine.image))
                        ],),
                       Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(medicine.description, style: TextStyle(color: Colors.white,),),
                          ),
                        ],),
                    ],
                    
                  ),
                  
                  ),
              ],
            ),
          )
        ],
      ),
    );
  }
}