import 'package:flutter/material.dart';
import 'package:lab_6/constant.dart';
import 'package:lab_6/models/products.dart';
import 'package:lab_6/screens/details/detail_page.dart';
// import 'package:lab_6/screens/details/detail_page.dart';
import 'item_card.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(kDefaultPaddin),
          child: Text(
            "Kamus Obat",
            style:TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                //.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
            child: GridView.builder(
            itemCount: medicines.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: kDefaultPaddin,
              crossAxisSpacing: kDefaultPaddin,
              childAspectRatio: 0.75,
            ), 
            itemBuilder: (context, index) => ItemCard(
              medicine: medicines[index], 
              press: () => Navigator.push(
                context, 
                MaterialPageRoute(builder: (context) => DetailPage(
                  medicine: medicines[index])))
            )),
          ) ,
        ),
      ],
    );
  }
}