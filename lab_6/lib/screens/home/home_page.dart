import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/screens/home/body.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppbar(),
        body: Body(),
    );
  }

  AppBar buildAppbar() {
    return AppBar(
       leading: Icon(Icons.arrow_back),
          title: Text(widget.title, style: TextStyle(color: Colors.yellow, fontWeight: FontWeight.bold)),
          centerTitle: true,
          actions: <Widget>[
            IconButton(onPressed: null,
                icon: new Icon(Icons.search, color: Colors.white)),
          ],
    );
  }
    Widget buildTitle() {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text("Kamus Obat", style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
    }

  Widget buildImageCard1() {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Card(
          child: Column(
            children: <Widget>[
              ListTile(
                leading: Hero(
                    tag: "https://images.tokopedia.net/img/cache/700/product-1/2020/2/7/391219/391219_aaa80ff8-792f-4aa4-9c07-bc16f718207d_554_554.jpg",
                    child: Image.network(
                        "https://images.tokopedia.net/img/cache/700/product-1/2020/2/7/391219/391219_aaa80ff8-792f-4aa4-9c07-bc16f718207d_554_554.jpg")
                ),
                title: Text("Paramex", style: TextStyle(
                    fontSize: 14, fontWeight: FontWeight.bold)),
                subtitle: Text("Obat Pereda Sakit Kepala"),

              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextButton(onPressed: () {}, child: const Text("Lihat Detail")
                  ),
                  const SizedBox(width: 8)

                ],
              )
            ],
          )
      ),

    );
  }
  Widget buildImageCard2() {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: Hero(
                    tag: "https://id-live-02.slatic.net/p/b471c419cedbe15c6f6e3d9fd030b9ed.jpg",
                    child: Image.network(
                        "https://id-live-02.slatic.net/p/b471c419cedbe15c6f6e3d9fd030b9ed.jpg")
                ),
                title: Text("Sanmol", style: TextStyle(
                    fontSize: 14, fontWeight: FontWeight.bold)),
                subtitle: Text("Obat Penurun Demam"),

              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextButton(onPressed: () {}, child: const Text("Lihat Detail")
                  ),
                  const SizedBox(width: 8)

                ],
              )
            ],
          )
      ),

    );
  }
  Widget buildImageCard3() {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: Hero(
                    tag: "https://assets.klikindomaret.com/share/10034123_1.jpg",
                    child: Image.network(
                        "https://assets.klikindomaret.com/share/10034123_1.jpg")
                ),
                title: Text("Salonpas", style: TextStyle(
                    fontSize: 14, fontWeight: FontWeight.bold)),
                subtitle: Text("Koyo Pereda Nyeri"),

              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextButton(onPressed: () {}, child: const Text("Lihat Detail")
                  ),
                  const SizedBox(width: 8)

                ],
              )
            ],
          )
      ),

    );
  }

}
