import 'package:flutter/material.dart';
import 'package:lab_6/models/products.dart';
import 'package:lab_6/constant.dart';

class ItemCard extends StatelessWidget {
  final Medicine medicine;
  final VoidCallback press;
  const ItemCard({
    required this.medicine,
    required this.press
  });
  Widget build(BuildContext context) {
    return GestureDetector(
      //onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
          child: Container(
            padding: EdgeInsets.all(kDefaultPaddin),
            //  height: 180,
            //  width: 160,
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(16),
            ),
            child: Image.network(medicine.image),
            ),
          ),
          Padding(padding: const EdgeInsets.symmetric(vertical: kDefaultPaddin/8),
            child: Column(
              children: [
                Text(
                medicine.title, textAlign: TextAlign.center,
                style: TextStyle(color: kTextColor, fontSize: 20),
                ),
                TextButton(onPressed: press, child: Text("Lihat Detail"),)
              ],
            )

          ),
          //  Padding(
          //   padding: const EdgeInsets.symmetric(vertical: 0),
          //   child: TextButton(onPressed: (){}, child: Text("Lihat Detail"),)
          // ),
        
      ],)
    );
  }
}