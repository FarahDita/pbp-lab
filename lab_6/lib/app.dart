import 'package:flutter/material.dart';
import 'package:lab_6/constant.dart';
import 'package:lab_6/screens/home/home_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lindungi Peduli',
      theme: ThemeData(
        primaryColor: Colors.cyan[800],
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,

      ),
      home: const HomePage(title: 'LindungiPeduli'),
    );
  }
}