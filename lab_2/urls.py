# Melakukan import dari views untuk dapat mengambil method yang bersesuaian
from django.urls import path
from .views import index, xml, json 

urlpatterns = [
    # membuat route path sesuai dengan permintaan pada soal untuk menampilkan:
    # HTML dengan method index
    path('', index, name='index'),
    # XML dengan method xml
    path('xml/', xml, name='xml'),
    # JSON dengan method json
    path('json/', json, name='json'),
]