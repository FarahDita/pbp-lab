from django.db import models

# Create your models here.
class Note(models.Model):
    # Menyusun model dari database yang berisi from, to, title, and message
    untuk = models.CharField(max_length=30, default='')
    oleh = models.CharField(max_length=30, default='')
    judul = models.CharField(max_length=30, default='')
    pesan = models.TextField(default="")
