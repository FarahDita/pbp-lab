from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
# melakukan convert object dengan serializer ke bentuk yang dapat ditransfer melalui internet
# menggunakan HTTP antara client dan server
from django.core import serializers

# Create your views here.

# Menampilkan data dalam bentuk HTML
def index(request):
    notes = Note.objects.all().values()
    response = {"notes" : notes}
    return render(request, 'lab2.html', response)

# Menampilkan data dalam bentuk XML
def xml(request):
    # serializer mengubah struktur data object menjadi XML
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

# Menampilkan data dalam bentuk JSON
def json(request):
    # serializer mengubah struktur data sesuai dengan format encoding JSON, yaitu string
    # serializer yang melakukan encode object ke string
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
