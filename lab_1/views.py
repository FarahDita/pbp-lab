from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = 'Farah Dita Ashilah'  # TODO: menuliskan nama mahasiswa
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 1, 10)  # TODO: memasukkan tanggal kelahiran mahasiswa
npm = 2006463723  # TODO: memasukkan NPM mahasiswa


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends =  Friend.objects.all().values() # TODO: meng-assign atribut dan behaviour yang ada pada class Friend kepada fariable friends
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
