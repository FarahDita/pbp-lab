from datetime import datetime
from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30, default='')
    # TODO Implement missing attributes in Friend model
    npm = models.CharField(max_length=10, default='') # menambahkan field untuk NPM dengan maksimal panjang 10 dan default string kosong
    dob = models.DateField(default=datetime.now) # menambahkan field untuk DOB dan default berupa tanggal hari ini
