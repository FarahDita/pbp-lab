from django.urls import path
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    # membuat path sesuai dengan yang tertera pada tests.py dan menambahkan teman menggunakan friend_list
    path('friends/', friend_list, name='friend_list')
]
