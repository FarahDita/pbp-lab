from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
# Create your views here.
from lab_1.models import Friend
from .forms import FriendForm

# Menggunakan login required, yang mana sebelum membuka template untuk function index/add_friend perlu login ke django terlebih dahulu
## Mengambil semua valued dari object Friend kemudian dibuat dalam bentuk dictionary
    ## Kemudian render ke html yang sesuai dengan request dan response yang sesuai
@login_required(login_url='/admin/login/')
def index(request):
    friends =  Friend.objects.all().values() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context ={}
    form = FriendForm(request.POST or None)
    ## mengecek terlebih dahulu apakah form yang dimasukkan user valid dan method yang diminta adalah POST
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-3') ## Setelah submit halaman akan di redirect ke friend list

    context['form']= form
    return render(request, "lab3_form.html", context)


