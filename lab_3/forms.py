from datetime import datetime
from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    ## menghubungkan logika kode yang mana fields dari models dapat dohubungkan ke widget pada forms
    class Meta:
        model = Friend
        fields = ['name',
        'npm',
        'dob',
        ]
    
    error_messages = {
		'required' : 'Please Type'
        }

    ## Membuat attribut untuk setiap field
    input_nama = {
		'type' : 'text',
		'placeholder' : 'NPM Kamu'
        }
    
    input_npm = {
		'type' : 'text',
		'placeholder' : 'NPM Kamu'
        }
    
    input_dob = {
		'type' : 'date',
		'placeholder' : 'Tanggal Lahir Kamu'
        }
    ## Membuat field dan widget dalam bentuk forms
    name = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=input_nama))
    npm = forms.CharField(label='', required=True, max_length=10, widget=forms.TextInput(attrs=input_npm))
    dob = forms.DateField(label='', required=True, widget=forms.DateInput(attrs=input_dob))
