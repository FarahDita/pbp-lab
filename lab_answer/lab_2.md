Nama	: Farah Dita Ashilah
Kelas	: PBP D
NPM	    : 2006463723

Sebelum menjawab soal yang diberikan, saya izin menyampaikan terlebih dahulu mengenai definisi JSON, XML, dan HTML.
JSON: 
merupakan singkatan dari JavaScript Object Notation format file yang menggunakan teks yang dapat dipahami manusia untuk menyimpandan mengirimkan object data yang berisi atribut. Singkatnya, JSON menawarkan koleksi data-data yang dapat diakses

XML:
merupakan singkatan dari Extensible Markup Language yang didesain untuk "membawa" data, baik dalam konteks menyimpan maupun mentransfer data. XML merupakan bahasa markup yang menyimpan data datam format teks yang sederhana sehingga dapat mengakomodasi berbagai bahasa manusia tanpa harus mengubah format sama sekali. Oleh karena itu, XML mudah untuk diperbaharui ke OS yang baru

HTML:
merupakan singkatan dari Hypertext Markup Language: bahasa markup yang membantu programmer untuk membuat dan mendesain konten web. HTML didesain utnuk menampilkan data dalam susunan format. HTML memiliki sejumlah tag dan atribut untuk menyusuk layout serta struktur dari sebuah dokumen web. Siapapun dapat mengedit kode HTML di editor manapun dan dapat mengeksekusi kode tersebut pada browser apapun.

Soal 1
Apakah perbedaan antara JSON dan XML?
JSON dan XML merupakan bentuk format yang bisa ditranspotasikan melalui internet. Kode program terdiri dari sejumlah atribut dan object, sehingga perlu mengalami konstruksi ulang pada strukturnya untuk mempermudah proses transfer. Biasanya menjadi dalam bentuk JSON atau XML. Berikut adalah beberapa perbedaan pada JSON dan XML:

JSON:
1. Merepresentasikan data dalam bentuk key:value pairs (lebih mudah dipahami manusia dan dapat dipahami mesin pula) dan memiliki struktur data seperti map
2. Tidak menyediakan namaspace support
3. Tidak memiliki kemapuan untuk menampilkan data
4. Lebih umum digunakan untuk data delivery antara server dan browser
5. Tidak menggunakan end tags
6. Tidak lebih aman dibandingkan dengan XML

XML:
1. Mereprentasikan data dalam bentuk named tag dan memiliki struktur data seperti tree
2. Menyediakan namespace support
3. Memiliki kemampuan untuk menampilkan data
4. Lebih umum digunakan untuk menyimpan informasi pada server-side
5. Menggunakan end tag
6. Lebih aman dibandingkan dengan JSON



Soal 2
Apakah perbedaan antara HTML dan XML?
HTML dan XML sama-sama menggunakan bahasa markup namunm memiliki fungsi yang berbeda, seperti sebagai berikut:

HTML:
1. Dapat menampilkan data
2. Menyediakan tag-tag tertentu yang memiliki fungsinya tersendiri untuk menjalankan program
3. Tidak case sensitive
4. Tidak menggunakan end tag
5. Diperbolehkan untuk menggunakan whitespace
6. Merupakan content-driven

XML:
1. Dapat menampilkan dan mengirimkan data
2. Pengguna dapat membuat tagnya sendiri
3. Case-sensitive
4. Menggunakan end tag
5. Tidak boleh menggunakan whitespace
6. Merupakan format driven


Sumber:
- https://www.geeksforgeeks.org/difference-between-json-and-xml/
- https://www.niagahoster.co.id/blog/xml/
- https://www.guru99.com/xml-vs-html-difference.html